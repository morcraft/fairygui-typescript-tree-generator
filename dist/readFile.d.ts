/// <reference types="node" />
import tsGenerator from './tsGenerator';
export default function readFile(this: tsGenerator, fileName: string, basePath: string): Buffer;
