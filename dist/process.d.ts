import tsGenerator from './tsGenerator';
export default function process(this: tsGenerator, outputPath: string, moduleName: string): void;
