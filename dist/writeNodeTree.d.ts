import tsGenerator from './tsGenerator';
export default function writeNodeTree(this: tsGenerator, element: Element, fileName: string, rootNode: boolean): Element[];
