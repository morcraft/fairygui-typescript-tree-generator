"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sanitizeURL_1 = require("./sanitizeURL");
function isValidURL(definitionsTree, url) {
    var sanitizedURL = sanitizeURL_1.default(url);
    return !!definitionsTree.nodeCollection[sanitizedURL];
}
exports.default = isValidURL;
