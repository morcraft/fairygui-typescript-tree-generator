"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var collectableProperties = {
    _children: 'Child',
    _transitions: 'Transition',
    _controllers: 'Controller'
};
exports.default = collectableProperties;
