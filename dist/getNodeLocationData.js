"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var isExternalComponentPath_1 = require("./isExternalComponentPath");
var sanitizeNodeName_1 = require("./sanitizeNodeName");
var sanitizeFileName_1 = require("./sanitizeFileName");
var chalk = require('chalk');
function getNodeLocationData(node, filePath) {
    var locationData = {
        tagName: node.tagName,
        nodeName: node.getAttribute('name'),
        externalPath: node.getAttribute('fileName'),
        url: node.getAttribute('url'),
        isExternalComponent: false
    };
    if (isExternalComponentPath_1.default(locationData.externalPath)) {
        locationData.isExternalComponent = true;
    }
    else {
        if (locationData.url && locationData.url.length) {
            if (!this.isValidURL(locationData.url))
                throw new Error("Invalid url " + chalk.red(locationData.url) + " in node with name " + locationData.nodeName + ". Aborting.");
            locationData.isExternalComponent = true;
            locationData.externalPath = this.getFilePathFromURL(locationData.url);
        }
    }
    locationData.sanitizedNodeName = sanitizeNodeName_1.default(locationData.nodeName);
    if (locationData.isExternalComponent) {
        locationData.sanitizedExternalPath = sanitizeFileName_1.default(locationData.externalPath);
        locationData.nodeKey = locationData.sanitizedExternalPath + "\u01C3" + locationData.sanitizedNodeName + "\u01C3" + locationData.tagName.toUpperCase();
    }
    else {
        if (!filePath)
            throw new Error("A path is required to form location data for element with name " + locationData.nodeName);
        locationData.nodeKey = filePath + "\u01C3" + locationData.sanitizedNodeName + "\u01C3" + locationData.tagName.toUpperCase();
    }
    return locationData;
}
exports.default = getNodeLocationData;
