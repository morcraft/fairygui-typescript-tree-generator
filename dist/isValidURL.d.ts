import Tree from './tree';
export default function isValidURL(definitionsTree: Tree, url: string): boolean;
