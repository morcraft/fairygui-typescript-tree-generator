"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var simpleNodeLogger = require('simple-node-logger'); //typings? :c
var loggerOptions_1 = require("./loggerOptions");
exports.loggerOptions = loggerOptions_1.default;
var xmldom = require("xmldom");
exports.xmldom = xmldom;
var tree_1 = require("./tree");
exports.Tree = tree_1.default;
var sanitizeFileName_1 = require("./sanitizeFileName");
exports.sanitizeFileName = sanitizeFileName_1.default;
var exportTreeAsTypescript_1 = require("./exportTreeAsTypescript");
var getItemFromDefinitionsPackage_1 = require("./getItemFromDefinitionsPackage");
exports.getItemFromDefinitionsPackage = getItemFromDefinitionsPackage_1.default;
var getFilePathFromURL_1 = require("./getFilePathFromURL");
exports.getFilePathFromURL = getFilePathFromURL_1.default;
var getNodeLocationData_1 = require("./getNodeLocationData");
var isValidURL_1 = require("./isValidURL");
exports.isValidURL = isValidURL_1.default;
var writeNodeTree_1 = require("./writeNodeTree");
exports.writeNodeTree = writeNodeTree_1.default;
var processEntryPoint_1 = require("./processEntryPoint");
var processDefinitionsPackage_1 = require("./processDefinitionsPackage");
var process_1 = require("./process");
var readFile_1 = require("./readFile");
var fairyGUIElementsMap_1 = require("./fairyGUIElementsMap");
exports.getFairyGUIElement = fairyGUIElementsMap_1.getElement;
var TypescriptGenerator = /** @class */ (function () {
    function TypescriptGenerator(basePath, entryPoint) {
        var _this = this;
        this.basePath = basePath;
        this.entryPoint = entryPoint;
        this.domParser = new xmldom.DOMParser;
        this.tagsMap = {};
        this.logger = simpleNodeLogger.createSimpleLogger(loggerOptions_1.default);
        this.getEntryPoint = function () { return _this.sanitizedEntryPoint; };
        this.isValidURL = function (url) { return isValidURL_1.default(_this.definitionsTree, url); };
        this.getItemFromDefinitionsPackage = function (url) { return getItemFromDefinitionsPackage_1.default(_this.definitionsTree, url); };
        this.getFilePathFromURL = function (url) { return getFilePathFromURL_1.default.call(_this, url); };
        this.getNodeLocationData = function (node, filePath) { return getNodeLocationData_1.default.call(_this, node, filePath); };
        this.writeNodeTree = function (element, fileName, rootNode) { return writeNodeTree_1.default.call(_this, element, fileName, rootNode); };
        this.processEntryPoint = function (fileName) { return processEntryPoint_1.default.call(_this, fileName); };
        this.readFile = function (fileName) { return readFile_1.default.call(_this, fileName, _this.basePath); };
        this.processDefinitionsPackage = function (fileName) { return processDefinitionsPackage_1.default.call(_this, fileName); };
        this.exportTreeAsTypescript = function (moduleName) { return exportTreeAsTypescript_1.default(moduleName, _this.elementsTree); };
        this.writeTypescriptFile = function (outputPath, moduleName) {
            return fs.writeFileSync(path.join(outputPath, moduleName + ".d.ts"), _this.exportTreeAsTypescript(moduleName));
        };
        this.process = function (outputPath, moduleName) { return process_1.default.call(_this, outputPath, moduleName); };
        this.sanitizedEntryPoint = sanitizeFileName_1.default(entryPoint);
        this.elementsTree = new tree_1.default(this.sanitizedEntryPoint);
    }
    return TypescriptGenerator;
}());
exports.default = TypescriptGenerator;
