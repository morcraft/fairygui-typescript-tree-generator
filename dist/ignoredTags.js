"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ignoredTags = {
    Button: true,
    relation: true,
    ComboBox: true,
    Slider: true,
    gearXY: true,
    group: true
};
exports.default = ignoredTags;
