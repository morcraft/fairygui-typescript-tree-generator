declare const map: {
    component: string;
    list: string;
    ComboBox: string;
    Slider: string;
    richtext: string;
    movieclip: string;
    transition: string;
    graph: string;
    relation: string;
    text: string;
    Button: string;
    loader: string;
    image: string;
    controller: string;
};
export declare type fairyGUIType = keyof typeof map;
export declare const extensions: {
    Button: string;
};
export declare function getElement(type: fairyGUIType, strict?: boolean): string;
export default map;
