"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getComponent_1 = require("./getComponent");
var chalk = require('chalk');
function processEntryPoint(fileName) {
    var fileBuffer = this.readFile(fileName);
    if (!(fileBuffer instanceof Buffer)) {
        this.logger.error(chalk.red("Invalid buffer returned from reading file " + fileName + ". This may result in a broken tree. Continuing..."));
        return [];
    }
    var XML = this.domParser.parseFromString(fileBuffer.toString());
    var component = getComponent_1.default(XML);
    var children = this.writeNodeTree(component, fileName, true);
    return children;
}
exports.default = processEntryPoint;
