"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getPackageDescription_1 = require("./getPackageDescription");
var tree_1 = require("./tree");
var chalk = require('chalk');
function processDefinitionsPackage(fileName) {
    var fileBuffer = this.readFile(fileName);
    if (!(fileBuffer instanceof Buffer)) {
        this.logger.error(chalk.red("Invalid buffer returned from reading file " + fileName + ". This may result in a broken tree. Continuing..."));
        return false;
    }
    var XML = this.domParser.parseFromString(fileBuffer.toString());
    var packageDescription = getPackageDescription_1.default(XML);
    if (!packageDescription)
        throw new Error("Couldn't find package description for fileName " + fileName);
    var packageId = packageDescription.getAttribute('id');
    if (!packageId || !packageId.length)
        throw new Error("Couldn't find id for package in folder " + this.basePath);
    this.packageId = packageId;
    this.definitionsTree = new tree_1.default(fileName);
    var resources = packageDescription.getElementsByTagName('resources')[0];
    for (var i = 0; i < resources.childNodes.length; i++) {
        var node = resources.childNodes[i];
        if (node.constructor.name == 'Element') {
            var nodeId = node.getAttribute('id');
            this.definitionsTree.addNode(node, this.packageId + nodeId);
        }
    }
    return true;
}
exports.default = processDefinitionsPackage;
