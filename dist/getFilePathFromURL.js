"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
function getFilePathFromURL(url) {
    var item = this.getItemFromDefinitionsPackage(url);
    var node = item;
    var fullPath = {
        path: node.getAttribute('path'),
        name: node.getAttribute('name'),
    };
    for (var prop in fullPath) {
        if (!fullPath[prop])
            throw new Error("Invalid " + prop + " for node with URL " + url + ".");
    }
    var filePath = fullPath.path[0] == '/' ? fullPath.path.substr(1) : fullPath.path;
    return path.join(filePath, fullPath.name);
}
exports.default = getFilePathFromURL;
