declare const ownerPropertyNames: {
    transition: string;
    controller: string;
    default: string;
};
export default ownerPropertyNames;
