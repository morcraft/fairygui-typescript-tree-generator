declare const loggerOptions: {
    logFilePath: string;
    timestampFormat: string;
};
export default loggerOptions;
