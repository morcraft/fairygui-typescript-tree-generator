"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getPackageDescription(document) {
    return document.getElementsByTagName('packageDescription')[0];
}
exports.default = getPackageDescription;
