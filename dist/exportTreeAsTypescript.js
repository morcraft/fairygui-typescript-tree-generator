"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sanitizeNodeName_1 = require("./sanitizeNodeName");
var fairyGUIElementsMap_1 = require("./fairyGUIElementsMap");
var collectableProperties_1 = require("./collectableProperties");
var elementsWithoutGetBy_1 = require("./elementsWithoutGetBy");
var ownerPropertyNames_1 = require("./ownerPropertyNames");
function exportTreeAsTypescript(moduleName, elementsTree) {
    var moduleCode = ["declare module " + moduleName + "{", '}'];
    var moduleInnerCode = [];
    if (Object.keys(elementsTree.nodeCollection).length) {
        moduleCode[0] += '\n';
        var _loop_1 = function (nodeName) {
            var treeNode = elementsTree.nodeCollection[nodeName];
            var tagName = treeNode.node.tagName;
            if (!tagName) {
                var errorMessage = "Couldn't determine tagName for node " + nodeName;
                throw new Error(errorMessage);
            }
            var extendedInterface = void 0;
            if (treeNode.inherits) {
                extendedInterface = treeNode.inherits;
            }
            else {
                var extension = treeNode.node.getAttribute('extention'); //not my typo. FairyGUI's.
                if (extension) {
                    extendedInterface = fairyGUIElementsMap_1.getElement(extension, true);
                }
                else {
                    extendedInterface = fairyGUIElementsMap_1.getElement(tagName);
                }
                extendedInterface = "fairygui." + extendedInterface;
            }
            var interfaceCode = ["\tinterface " + nodeName + " extends " + extendedInterface + "{", "}"];
            var innerInterfaceCode = [];
            var empty = true;
            var _loop_2 = function (prop) {
                var collection = treeNode[prop];
                if (Array.isArray(collection)) {
                    if (empty) {
                        empty = false;
                        interfaceCode[0] += '\n';
                        interfaceCode[1] = '\t' + interfaceCode[1];
                    }
                    var names_1 = [];
                    collection.forEach(function (item, index) {
                        var nodeName = item.nodeName;
                        names_1.push(nodeName);
                        var fairyGUIName = sanitizeNodeName_1.default(item.node.getAttribute('name'));
                        var fairyGUIId = item.node.getAttribute('id');
                        if (!elementsWithoutGetBy_1.default[tagName]) { //elements like lists don't provide getElementBy methods
                            innerInterfaceCode.push("\t\tget" + collectableProperties_1.default[prop] + "(name: '" + fairyGUIName + "'): " + nodeName + "\n");
                            innerInterfaceCode.push("\t\tget" + collectableProperties_1.default[prop] + "At(index: " + index + "): " + nodeName + "\n");
                            if (prop == '_children') {
                                innerInterfaceCode.push("\t\tgetChildById(id: '" + fairyGUIId + "'): " + nodeName + "\n");
                            }
                        }
                    });
                    innerInterfaceCode.push("\t\t" + prop + ": [\n\t\t\t" + names_1.join(',\n\t\t\t') + "\n\t\t]\n");
                }
            };
            for (var prop in collectableProperties_1.default) {
                _loop_2(prop);
            }
            if (treeNode.parentName) {
                if (empty) {
                    empty = false;
                    interfaceCode[0] += '\n';
                    interfaceCode[1] = '\t' + interfaceCode[1];
                }
                var ownerProperty = ownerPropertyNames_1.default[tagName] ? ownerPropertyNames_1.default[tagName] : ownerPropertyNames_1.default.default;
                innerInterfaceCode.push("\t\t" + ownerProperty + ": " + treeNode.parentName + "\n");
            }
            moduleInnerCode.push(interfaceCode[0] + innerInterfaceCode.join('') + interfaceCode[1] + '\n');
        };
        for (var nodeName in elementsTree.nodeCollection) {
            _loop_1(nodeName);
        }
    }
    return moduleCode[0] + moduleInnerCode.join('') + moduleCode[1];
}
exports.default = exportTreeAsTypescript;
