import tsGenerator from './tsGenerator';
export default function processDefinitionsPackage(this: tsGenerator, fileName: string): boolean;
