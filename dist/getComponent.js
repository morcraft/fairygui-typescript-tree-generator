"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getComponent(document) {
    return document.getElementsByTagName('component')[0];
}
exports.default = getComponent;
