import Tree from './tree';
export default function getItemFromDefinitionsPackage(definitionsTree: Tree, url: string): Element;
