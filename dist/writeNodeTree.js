"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sanitizeFileName_1 = require("./sanitizeFileName");
var ignoredTags_1 = require("./ignoredTags");
var getElementChildNodes_1 = require("./getElementChildNodes");
function writeNodeTree(element, fileName, rootNode) {
    var _this = this;
    var sanitizedFileName = sanitizeFileName_1.default(fileName); //make it readable
    var children = [];
    //console.log(`fileName: ${chalk.cyanBright(fileName)} Root node ${chalk.cyanBright(rootNode)}`)
    if (rootNode) {
        children.push.apply(children, this.writeNodeTree(element, sanitizedFileName, false));
        return children;
    }
    this.elementsTree.addNode(element, sanitizedFileName);
    var nodes = getElementChildNodes_1.default(element);
    nodes.forEach(function (node) {
        //console.log('node', node.toString())
        var tagName = node.tagName;
        if (tagName == 'displayList') { //displayList elements don't exist during runtime, but their childNodes are added as children of the component they appear in
            children.push.apply(//displayList elements don't exist during runtime, but their childNodes are added as children of the component they appear in
            children, _this.writeNodeTree(node, sanitizedFileName, false));
            return true;
        }
        _this.tagsMap[tagName] = true; //add tag for reference
        if (ignoredTags_1.default[tagName])
            return true; //skip ignored tags
        var locationData = _this.getNodeLocationData(node, fileName);
        if (locationData.isExternalComponent) {
            //console.log(`External component with path ${chalk.cyanBright(locationData.externalPath)} and tagName ${chalk.cyanBright(tagName)}`)
            if (tagName == 'component' || tagName == 'item') {
                children.push(_this.processEntryPoint(locationData.externalPath));
                _this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName, locationData.sanitizedExternalPath);
            }
            else {
                _this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName);
            }
            return true;
        }
        if (!locationData.nodeName || !locationData.nodeName.length) {
            //this.logger.info(`Invalid name in node with index ${key} in displayList in component ${sanitizedFileName}`)
            return true;
        }
        _this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName);
        if (tagName == 'list') //lists have their own URL-referenced elements
            children.push.apply(//lists have their own URL-referenced elements
            children, _this.writeNodeTree(node, locationData.nodeKey, false));
    });
    return children;
}
exports.default = writeNodeTree;
