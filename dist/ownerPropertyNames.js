"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ownerPropertyNames = {
    transition: '_owner',
    controller: '_parent',
    default: 'parent'
};
exports.default = ownerPropertyNames;
