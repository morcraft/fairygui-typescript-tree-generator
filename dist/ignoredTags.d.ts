declare const ignoredTags: {
    Button: boolean;
    relation: boolean;
    ComboBox: boolean;
    Slider: boolean;
    gearXY: boolean;
    group: boolean;
};
export declare type keys = keyof typeof ignoredTags;
export default ignoredTags;
