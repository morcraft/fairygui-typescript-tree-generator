"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var map = {
    component: 'GComponent',
    list: 'GList',
    ComboBox: 'GComboBox',
    Slider: 'GSlider',
    richtext: 'GRichTextField',
    movieclip: 'GMovieClip',
    transition: 'Transition',
    graph: 'GGraph',
    relation: 'GRelation',
    text: 'GBasicTextField',
    Button: 'GButton',
    loader: 'GLoader',
    image: 'GImage',
    controller: 'Controller'
};
exports.extensions = {
    Button: 'GButton'
};
function getElement(type, strict) {
    if (!map[type]) {
        if (strict)
            throw new Error("Couldn't determine fairyGUIType for type " + type);
        console.log("Unkown type " + type);
        return 'UNKNOWN_TYPE';
    }
    return map[type];
}
exports.getElement = getElement;
exports.default = map;
