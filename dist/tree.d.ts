export declare const specialNodes: {
    transition: string;
    controller: string;
};
export declare type NodeMap = {
    node: Element;
    parent?: Element;
    parentName?: string;
};
export interface TreeNode {
    node: Element;
    nodeName: string;
    parent?: Element;
    parentName?: string;
    inherits?: string;
    _children?: TreeNode[];
    _transitions?: TreeNode[];
    _controllers?: TreeNode[];
}
export interface NodeCollection {
    [s: string]: TreeNode;
}
export default class Tree {
    readonly entryComponent: string;
    nodeCollection: NodeCollection;
    constructor(entryComponent: string);
    addChild(node: Element, nodeName: string, parent?: Element, parentName?: string): void;
    addNode(node: Element, nodeName: string, parent?: Element, parentName?: string, inherits?: string): void;
}
