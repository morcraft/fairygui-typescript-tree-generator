import NodeLocationData from './NodeLocationData';
import tsGenerator from './tsGenerator';
export default function getNodeLocationData(this: tsGenerator, node: Element, filePath?: string): NodeLocationData;
