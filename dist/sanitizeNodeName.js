"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function sanitizeNodeName(nodeName) {
    if (typeof nodeName != 'string')
        return '';
    return nodeName
        .replace('-', '_')
        .replace('.xml', '');
}
exports.default = sanitizeNodeName;
