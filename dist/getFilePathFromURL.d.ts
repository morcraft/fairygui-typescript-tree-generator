import tsGenerator from './tsGenerator';
export default function getFilePathFromURL(this: tsGenerator, url: string): string;
