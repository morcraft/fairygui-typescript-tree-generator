import tsGenerator from './tsGenerator';
export default function processEntryPoint(this: tsGenerator, fileName: string): Element[];
