"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isExternalComponentPath(path) {
    return path && path.lastIndexOf('.xml') === path.length - '.xml'.length;
}
exports.default = isExternalComponentPath;
