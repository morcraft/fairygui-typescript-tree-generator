"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sanitizeURL_1 = require("./sanitizeURL");
function getItemFromDefinitionsPackage(definitionsTree, url) {
    var sanitizedURL = sanitizeURL_1.default(url);
    var item = definitionsTree.nodeCollection[sanitizedURL];
    if (!item || !item.node)
        throw new Error("Invalid url " + url + ". Couldn't retrieve element from the current tree.");
    return item.node;
}
exports.default = getItemFromDefinitionsPackage;
