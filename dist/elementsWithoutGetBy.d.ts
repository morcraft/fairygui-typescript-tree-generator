declare const elementsWithoutGetBy: {
    list: boolean;
};
export declare type type = keyof typeof elementsWithoutGetBy;
export default elementsWithoutGetBy;
