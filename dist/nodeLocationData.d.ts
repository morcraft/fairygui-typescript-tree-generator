export default interface NodeLocationData {
    nodeName: string;
    nodeKey: string;
    externalPath: string;
    sanitizedExternalPath: string;
    sanitizedNodeName: string;
    url: string;
    isExternalComponent: boolean;
}
