"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.specialNodes = {
    transition: '_transitions',
    controller: '_controllers'
};
var Tree = /** @class */ (function () {
    function Tree(entryComponent) {
        this.entryComponent = entryComponent;
        this.nodeCollection = {};
    }
    Tree.prototype.addChild = function (node, nodeName, parent, parentName) {
        var key = exports.specialNodes[node.nodeName] ? exports.specialNodes[node.nodeName] : '_children';
        var child = {
            nodeName: nodeName,
            node: node,
        };
        if (Array.isArray(this.nodeCollection[parentName][key]))
            this.nodeCollection[parentName][key].push(child);
        else
            this.nodeCollection[parentName][key] = [child];
    };
    Tree.prototype.addNode = function (node, nodeName, parent, parentName, inherits) {
        if (!nodeName)
            throw new Error('Invalid node name');
        if (parent) {
            if (!parentName)
                throw new Error('Invalid parent name for node' + parent);
            if (!this.nodeCollection[parentName])
                throw new Error("Parents must be added first in order to process tree. Given parentName " + parentName);
            if (!this.nodeCollection[nodeName]) {
                this.nodeCollection[nodeName] = {
                    node: node,
                    parentName: parentName,
                    nodeName: nodeName,
                    parent: parent,
                };
                if (inherits) {
                    this.nodeCollection[nodeName].inherits = inherits;
                }
                this.addChild(node, nodeName, parent, parentName);
            }
            else {
                //console.info(`Duplicated nodeName ${nodeName}. Skipping.`)
            }
            return;
        }
        if (!this.nodeCollection[nodeName]) {
            this.nodeCollection[nodeName] = {
                node: node,
                nodeName: nodeName,
            };
        }
        else {
            //console.info(`Duplicated nodeName ${nodeName}. Skipping.`)
        }
    };
    return Tree;
}());
exports.default = Tree;
