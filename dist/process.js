"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require('chalk');
function process(outputPath, moduleName) {
    var startTime = new Date();
    var packageFileName = 'Package.xml';
    this.logger.info('Processing definitions package...');
    this.processDefinitionsPackage(packageFileName);
    this.logger.info(chalk.cyan('Starting operation...'));
    this.logger.info("Using " + chalk.cyanBright(this.basePath) + " as FairyGUI's project path");
    this.logger.info("Processing entry point " + chalk.cyanBright(this.entryPoint) + "...");
    this.processEntryPoint(this.entryPoint);
    this.logger.info("Saving tree TypeScript definitions in " + chalk.cyanBright(outputPath) + "...");
    this.writeTypescriptFile(outputPath, moduleName);
    var endTime = new Date() - startTime;
    this.logger.info("Tree keys: " + chalk.cyanBright(Object.keys(this.elementsTree.nodeCollection).length));
    this.logger.info("Execution time: " + chalk.yellowBright(endTime + 'ms'));
}
exports.default = process;
