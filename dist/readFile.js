"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var chalk = require('chalk');
function readFile(fileName, basePath) {
    var buffer;
    try {
        buffer = fs.readFileSync(path.join(basePath, fileName));
    }
    catch (error) {
        this.logger.error(chalk.red("" + error));
    }
    return buffer;
}
exports.default = readFile;
