"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function sanitizeFileName(fileName) {
    if (typeof fileName != 'string')
        return '';
    return fileName
        .replace('.', '_')
        .replace('xml', 'XML')
        .replace('-', '_')
        .replace(/\//g, 'ǁ')
        .replace(/\\/g, 'ǁ');
}
exports.default = sanitizeFileName;
