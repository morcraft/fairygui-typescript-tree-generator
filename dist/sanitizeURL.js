"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function sanitizeURL(url) {
    return url.indexOf('ui://') > -1 ? url.split('ui://')[1] : url;
}
exports.default = sanitizeURL;
