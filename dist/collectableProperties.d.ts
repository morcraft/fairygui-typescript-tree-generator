declare const collectableProperties: {
    _children: string;
    _transitions: string;
    _controllers: string;
};
export default collectableProperties;
