"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getElementChildNodes(component) {
    var nodes = [];
    for (var i = 0; i < component.childNodes.length; i++) {
        if (component.childNodes[i].constructor.name == 'Element')
            nodes.push(component.childNodes[i]);
    }
    return nodes;
}
exports.default = getElementChildNodes;
