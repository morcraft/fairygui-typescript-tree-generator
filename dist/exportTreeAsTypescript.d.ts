import Tree from './tree';
export default function exportTreeAsTypescript(moduleName: string, elementsTree: Tree): string;
