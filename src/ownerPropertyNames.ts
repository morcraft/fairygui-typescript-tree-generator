const ownerPropertyNames = {
    transition: '_owner',
    controller: '_parent',
    default: 'parent'
}

export default ownerPropertyNames