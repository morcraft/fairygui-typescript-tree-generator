import getComponent from "./getComponent";
import tsGenerator from './tsGenerator'
const chalk = require('chalk')

export default function processEntryPoint(this: tsGenerator, fileName: string): Element[]{
    const fileBuffer = this.readFile(fileName)

    if(!(fileBuffer instanceof Buffer)){
        this.logger.error(chalk.red(`Invalid buffer returned from reading file ${fileName}. This may result in a broken tree. Continuing...`))
        return []
    }

    const XML = this.domParser.parseFromString(fileBuffer.toString())
    const component = getComponent(XML)
    const children = this.writeNodeTree(component, fileName, true)
    return children
}