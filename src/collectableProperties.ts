const collectableProperties = {
    _children: 'Child',
    _transitions: 'Transition',
    _controllers: 'Controller'
}

export default collectableProperties