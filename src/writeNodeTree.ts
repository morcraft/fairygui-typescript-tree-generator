import sanitizeFileName from './sanitizeFileName'
import ignoredTags from './ignoredTags'
import getElementChildNodes from './getElementChildNodes'
import tsGenerator from './tsGenerator'

export default function writeNodeTree(this: tsGenerator, element: Element, fileName: string, rootNode: boolean): Element[]{
    const sanitizedFileName = sanitizeFileName(fileName) //make it readable
    const children = []

    //console.log(`fileName: ${chalk.cyanBright(fileName)} Root node ${chalk.cyanBright(rootNode)}`)
    if(rootNode){
        children.push(...this.writeNodeTree(element, sanitizedFileName, false))      
        return children
    }
    
    this.elementsTree.addNode(element, sanitizedFileName)
    const nodes = getElementChildNodes(element)
    nodes.forEach(node => {
        //console.log('node', node.toString())
        const tagName = node.tagName

        if(tagName == 'displayList'){//displayList elements don't exist during runtime, but their childNodes are added as children of the component they appear in
            children.push(...this.writeNodeTree(node, sanitizedFileName, false))
            return true
        }

        this.tagsMap[tagName] = true//add tag for reference
        if(ignoredTags[tagName]) return true//skip ignored tags
        const locationData = this.getNodeLocationData(node, fileName)
        
        if(locationData.isExternalComponent){
            //console.log(`External component with path ${chalk.cyanBright(locationData.externalPath)} and tagName ${chalk.cyanBright(tagName)}`)
            if(tagName == 'component' || tagName == 'item'){
                children.push(this.processEntryPoint(locationData.externalPath))
                this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName, locationData.sanitizedExternalPath)
            }
            else{
                this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName)
            }

            return true
        }

        if(!locationData.nodeName || !locationData.nodeName.length){
            //this.logger.info(`Invalid name in node with index ${key} in displayList in component ${sanitizedFileName}`)
            return true
        }

        this.elementsTree.addNode(node, locationData.nodeKey, element, sanitizedFileName)

        if(tagName == 'list')//lists have their own URL-referenced elements
            children.push(...this.writeNodeTree(node, locationData.nodeKey, false))
    })
    
    return children
}