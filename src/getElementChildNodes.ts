export default function getElementChildNodes(component: Element): Element[]{
    const nodes = []
    for(let i = 0; i < component.childNodes.length; i++){
        if(component.childNodes[i].constructor.name == 'Element')
            nodes.push(component.childNodes[i])
    }

    return nodes
}