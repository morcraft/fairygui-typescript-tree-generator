import sanitizeURL from "./sanitizeURL";
import Tree from './tree'

export default function getItemFromDefinitionsPackage(definitionsTree: Tree, url: string): Element{
    const sanitizedURL = sanitizeURL(url)
    const item = definitionsTree.nodeCollection[sanitizedURL]
    if(!item || !item.node)
        throw new Error(`Invalid url ${url}. Couldn't retrieve element from the current tree.`)

    return item.node
}