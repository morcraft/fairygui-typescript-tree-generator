
export default function isExternalComponentPath(path: string){
    return path && path.lastIndexOf('.xml') === path.length - '.xml'.length
}