import path = require('path')
import tsGenerator from './tsGenerator'
const chalk = require('chalk')

export default function process(this: tsGenerator, outputPath: string, moduleName: string){
    const startTime = new Date() as any
    const packageFileName = 'Package.xml'
    this.logger.info('Processing definitions package...')
    this.processDefinitionsPackage(packageFileName)
    this.logger.info(chalk.cyan('Starting operation...'))
    this.logger.info(`Using ${chalk.cyanBright(this.basePath)} as FairyGUI's project path`)
    this.logger.info(`Processing entry point ${chalk.cyanBright(this.entryPoint)}...`)
    this.processEntryPoint(this.entryPoint)
    this.logger.info(`Saving tree TypeScript definitions in ${chalk.cyanBright(outputPath)}...`)
    this.writeTypescriptFile(outputPath, moduleName)
    const endTime: any = new Date() as any - startTime
    this.logger.info(`Tree keys: ${chalk.cyanBright(Object.keys(this.elementsTree.nodeCollection).length)}`)
    this.logger.info(`Execution time: ${chalk.yellowBright(endTime + 'ms')}`)
}
