import sanitizeURL from './sanitizeURL'
import Tree from './tree'

export default function isValidURL(definitionsTree: Tree, url: string){
    const sanitizedURL = sanitizeURL(url)
    return !!definitionsTree.nodeCollection[sanitizedURL]
}