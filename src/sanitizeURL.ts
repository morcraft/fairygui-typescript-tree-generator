export default function sanitizeURL(url: string){
    return url.indexOf('ui://') > -1 ? url.split('ui://')[1] : url
}