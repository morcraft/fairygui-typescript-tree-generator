export default function getPackageDescription(document: Document | Element){
    return document.getElementsByTagName('packageDescription')[0]
}