import NodeLocationData from './NodeLocationData'
import isExternalComponentPath from './isExternalComponentPath'
import tsGenerator from './tsGenerator'
import sanitizeNodeName from './sanitizeNodeName'
import sanitizeFileName from './sanitizeFileName'
const chalk = require('chalk')

export default function getNodeLocationData(this: tsGenerator, node: Element, filePath?: string): NodeLocationData {
    const locationData: any = {
        tagName: node.tagName,
        nodeName: node.getAttribute('name'),
        externalPath: node.getAttribute('fileName'),
        url: node.getAttribute('url'),
        isExternalComponent: false
    }

    if(isExternalComponentPath(locationData.externalPath)){
        locationData.isExternalComponent = true
    }
    else{
        if(locationData.url && locationData.url.length){
            if(!this.isValidURL(locationData.url))
                throw new Error(`Invalid url ${chalk.red(locationData.url)} in node with name ${locationData.nodeName}. Aborting.`)

            locationData.isExternalComponent = true
            locationData.externalPath = this.getFilePathFromURL(locationData.url)
        }
    }

    locationData.sanitizedNodeName = sanitizeNodeName(locationData.nodeName)

    if(locationData.isExternalComponent){
        locationData.sanitizedExternalPath = sanitizeFileName(locationData.externalPath)
        locationData.nodeKey = `${locationData.sanitizedExternalPath}ǃ${locationData.sanitizedNodeName}ǃ${locationData.tagName.toUpperCase()}`
    }
    else{
        if(!filePath)
            throw new Error(`A path is required to form location data for element with name ${locationData.nodeName}`)

        locationData.nodeKey = `${filePath}ǃ${locationData.sanitizedNodeName}ǃ${locationData.tagName.toUpperCase()}`
    }

    return locationData
}