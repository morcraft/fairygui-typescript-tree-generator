import fs = require('fs')
import path = require('path')
import tsGenerator from './tsGenerator'
const chalk = require('chalk')

export default function readFile(this: tsGenerator, fileName: string, basePath: string): Buffer{
    let buffer: Buffer
    try{
        buffer = fs.readFileSync(path.join(basePath, fileName))
    }
    catch(error){
        this.logger.error(chalk.red(`${error}`))
    }
    return buffer
}