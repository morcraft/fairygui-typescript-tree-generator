import tsGenerator from './tsGenerator'
import path = require('path')

export default function getFilePathFromURL(this: tsGenerator, url: string) : string{
    const item = this.getItemFromDefinitionsPackage(url)
    const node = item

    const fullPath = {
        path: node.getAttribute('path'),
        name: node.getAttribute('name'),
    }

    for(const prop in fullPath){
        if(!fullPath[prop])
            throw new Error(`Invalid ${prop} for node with URL ${url}.`)
    }

    const filePath = fullPath.path[0] == '/' ? fullPath.path.substr(1) : fullPath.path
    return path.join(filePath, fullPath.name)
}