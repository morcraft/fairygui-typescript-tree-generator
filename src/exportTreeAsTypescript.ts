import Tree from './tree'
import sanitizeNodeName from './sanitizeNodeName'
import { getElement as getFairyGUIElement, fairyGUIType } from './fairyGUIElementsMap'
import collectableProperties from './collectableProperties'
import elementsWithoutGetBy from './elementsWithoutGetBy'
import ownerPropertyNames from './ownerPropertyNames';

export default function exportTreeAsTypescript(moduleName: string, elementsTree: Tree){
    const moduleCode = [`declare module ${moduleName}{`, '}']
    const moduleInnerCode = []
    
    if(Object.keys(elementsTree.nodeCollection).length){
        moduleCode[0] += '\n'

        for(const nodeName in elementsTree.nodeCollection){
            const treeNode = elementsTree.nodeCollection[nodeName]
            let tagName = treeNode.node.tagName as fairyGUIType

            if(!tagName){
                const errorMessage = `Couldn't determine tagName for node ${nodeName}`
                throw new Error(errorMessage)
            }

            let extendedInterface

            if(treeNode.inherits){
                extendedInterface = treeNode.inherits
            }
            else{
                const extension = treeNode.node.getAttribute('extention')//not my typo. FairyGUI's.
                if(extension){
                    extendedInterface = getFairyGUIElement(extension as fairyGUIType, true)
                }
                else{
                    extendedInterface = getFairyGUIElement(tagName)
                }
                
                extendedInterface = `fairygui.${extendedInterface}`
            }

            const interfaceCode = [`\tinterface ${nodeName} extends ${extendedInterface}{`, `}`]
            const innerInterfaceCode = []
            let empty = true

            for(const prop in collectableProperties){
                const collection = treeNode[prop]
                if(Array.isArray(collection)){
                    if(empty){
                        empty = false
                        interfaceCode[0] += '\n'
                        interfaceCode[1] = '\t' + interfaceCode[1]
                    }
                    
                    const names = []
                    collection.forEach((item, index) => {
                        const nodeName = item.nodeName
                        names.push(nodeName)
                        const fairyGUIName = sanitizeNodeName(item.node.getAttribute('name'))
                        const fairyGUIId = item.node.getAttribute('id')

                        if(!elementsWithoutGetBy[tagName]){//elements like lists don't provide getElementBy methods
                            innerInterfaceCode.push(`\t\tget${collectableProperties[prop]}(name: '${fairyGUIName}'): ${nodeName}\n`)
                            innerInterfaceCode.push(`\t\tget${collectableProperties[prop]}At(index: ${index}): ${nodeName}\n`)

                            if(prop == '_children'){
                                innerInterfaceCode.push(`\t\tgetChildById(id: '${fairyGUIId}'): ${nodeName}\n`)
                            }
                        }

                    })

                    innerInterfaceCode.push(`\t\t${prop}: [\n\t\t\t${names.join(',\n\t\t\t')}\n\t\t]\n`)
                }
            }

            if(treeNode.parentName){
                if(empty){
                    empty = false
                    interfaceCode[0] += '\n'
                    interfaceCode[1] = '\t' + interfaceCode[1]
                }
                const ownerProperty = ownerPropertyNames[tagName] ? ownerPropertyNames[tagName] : ownerPropertyNames.default
                innerInterfaceCode.push(`\t\t${ownerProperty}: ${treeNode.parentName}\n`)
            }

            moduleInnerCode.push(interfaceCode[0] + innerInterfaceCode.join('') + interfaceCode[1] + '\n')
        }
    }
    
    return moduleCode[0] + moduleInnerCode.join('') + moduleCode[1]
}