const map = {
    component: 'GComponent',
    list: 'GList',
    ComboBox: 'GComboBox',
    Slider: 'GSlider',
    richtext: 'GRichTextField',
    movieclip: 'GMovieClip',
    transition: 'Transition',
    graph: 'GGraph',
    relation: 'GRelation',
    text: 'GBasicTextField',
    Button: 'GButton',
    loader: 'GLoader',
    image: 'GImage',
    controller: 'Controller'       
}

export type fairyGUIType = keyof typeof map

export const extensions = {
    Button: 'GButton'
}

export function getElement(type: fairyGUIType, strict?: boolean){
    if(!map[type]){
        if(strict)
            throw new Error(`Couldn't determine fairyGUIType for type ${type}`)

        console.log(`Unkown type ${type}`)
        return 'UNKNOWN_TYPE'
    }

    return map[type]
}

export default map