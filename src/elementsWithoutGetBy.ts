const elementsWithoutGetBy = {
    list: true
}

export type type = keyof typeof elementsWithoutGetBy

export default elementsWithoutGetBy