export default function sanitizeFileName(fileName: string){
    if(typeof fileName != 'string') return ''
    return fileName
        .replace('.', '_')
        .replace('xml', 'XML')
        .replace('-', '_')
        .replace(/\//g, 'ǁ')
        .replace(/\\/g, 'ǁ')
} 