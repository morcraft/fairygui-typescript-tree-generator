export const specialNodes = {
    transition: '_transitions',
    controller: '_controllers'
}

export type NodeMap = {
    node: Element
    parent?: Element
    parentName?: string
}

export interface TreeNode{
    node: Element
    nodeName: string
    parent?: Element
    parentName?: string
    inherits?: string
    _children?: TreeNode[]
    _transitions?: TreeNode[]
    _controllers?: TreeNode[]
}

export interface NodeCollection{
    [s: string]: TreeNode
}

export default class Tree{
    public nodeCollection: NodeCollection = {}
    constructor(readonly entryComponent: string){}

    addChild(node: Element, nodeName: string, parent?: Element, parentName?: string){
        const key = specialNodes[node.nodeName] ? specialNodes[node.nodeName] : '_children'
        const child = {
            nodeName,
            node,
        }

        if(Array.isArray(this.nodeCollection[parentName][key]))
            this.nodeCollection[parentName][key].push(child)
        else
            this.nodeCollection[parentName][key] = [child]

    }

    addNode(node: Element, nodeName: string, parent?: Element, parentName?: string, inherits?: string){
        if(!nodeName)
            throw new Error('Invalid node name')

        if(parent){
            if(!parentName)
                throw new Error('Invalid parent name for node' + parent)

            if(!this.nodeCollection[parentName])
                throw new Error(`Parents must be added first in order to process tree. Given parentName ${parentName}`)

            if(!this.nodeCollection[nodeName]){
                this.nodeCollection[nodeName] = {
                    node,
                    parentName,
                    nodeName,
                    parent,
                }
    
                if(inherits){
                    this.nodeCollection[nodeName].inherits = inherits
                }
                
                this.addChild(node, nodeName, parent, parentName)
            }

            else{
                //console.info(`Duplicated nodeName ${nodeName}. Skipping.`)
            }

            return
        }

        if(!this.nodeCollection[nodeName]){
            this.nodeCollection[nodeName] = {
                node,
                nodeName,
            }
        }
        else{
            //console.info(`Duplicated nodeName ${nodeName}. Skipping.`)
        }
    }
}