export default function sanitizeNodeName(nodeName: string): string {
    if(typeof nodeName != 'string') return ''
    return nodeName
        .replace('-', '_')
        .replace('.xml', '')
}