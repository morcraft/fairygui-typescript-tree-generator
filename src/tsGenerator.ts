import fs = require('fs')
import path = require('path')
const simpleNodeLogger = require('simple-node-logger')//typings? :c
import loggerOptions from './loggerOptions'
import xmldom = require('xmldom')
import Tree from './tree'
import sanitizeFileName from './sanitizeFileName'
import exportTreeAsTypescript from './exportTreeAsTypescript'
import getItemFromDefinitionsPackage from './getItemFromDefinitionsPackage'
import getFilePathFromURL from './getFilePathFromURL'
import getNodeLocationData from './getNodeLocationData'
import isValidURL from './isValidURL'
import writeNodeTree from './writeNodeTree'
import processEntryPoint from './processEntryPoint'
import processDefinitionsPackage from './processDefinitionsPackage'
import process from './process'
import NodeLocationData from './NodeLocationData'
import readFile from './readFile'
import { TreeNode, NodeCollection, NodeMap } from './tree'
import { getElement as getFairyGUIElement, fairyGUIType } from './fairyGUIElementsMap'

export default class TypescriptGenerator{
    domParser = new xmldom.DOMParser
    elementsTree: Tree
    definitionsTree: Tree
    tagsMap: any = {}
    sanitizedEntryPoint: string
    packageId: string
    logger = simpleNodeLogger.createSimpleLogger(loggerOptions)
    
    constructor(readonly basePath: string, readonly entryPoint: string){ 
        this.sanitizedEntryPoint = sanitizeFileName(entryPoint)
        this.elementsTree = new Tree(this.sanitizedEntryPoint)
    }

    getEntryPoint                 = () => this.sanitizedEntryPoint
    isValidURL                    = (url: string): boolean => isValidURL(this.definitionsTree, url)
    getItemFromDefinitionsPackage = (url: string): Element => getItemFromDefinitionsPackage(this.definitionsTree, url)
    getFilePathFromURL            = (url: string): string => getFilePathFromURL.call(this, url)
    getNodeLocationData           = (node: Element, filePath?: string): NodeLocationData => getNodeLocationData.call(this, node, filePath)
    writeNodeTree                 = (element: Element, fileName: string, rootNode: boolean): Element[] => writeNodeTree.call(this, element, fileName, rootNode)
    processEntryPoint             = (fileName: string): Element[] => processEntryPoint.call(this, fileName)
    readFile                      = (fileName: string) => readFile.call(this, fileName, this.basePath)
    processDefinitionsPackage     = (fileName: string) => processDefinitionsPackage.call(this, fileName)
    exportTreeAsTypescript        = (moduleName: string) => exportTreeAsTypescript(moduleName, this.elementsTree)
    writeTypescriptFile           = (outputPath: string, moduleName: string) => {
        return fs.writeFileSync(path.join(outputPath, `${moduleName}.d.ts`), this.exportTreeAsTypescript(moduleName))
    }
    process                       = (outputPath: string, moduleName: string): void => process.call(this, outputPath, moduleName)
}

export { loggerOptions, xmldom, Tree, sanitizeFileName, getItemFromDefinitionsPackage, getFilePathFromURL }
export { isValidURL, writeNodeTree, NodeLocationData, NodeCollection, NodeMap, TreeNode }
export { getFairyGUIElement, fairyGUIType }
