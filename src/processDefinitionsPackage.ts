import tsGenerator from './tsGenerator'
import getPackageDescription from "./getPackageDescription"
import Tree from './tree'
const chalk = require('chalk')

export default function processDefinitionsPackage(this: tsGenerator, fileName: string){
    const fileBuffer = this.readFile(fileName)

    if(!(fileBuffer instanceof Buffer)){
        this.logger.error(chalk.red(`Invalid buffer returned from reading file ${fileName}. This may result in a broken tree. Continuing...`))
        return false
    }

    const XML = this.domParser.parseFromString(fileBuffer.toString())
    const packageDescription = getPackageDescription(XML)
    
    if(!packageDescription)
        throw new Error(`Couldn't find package description for fileName ${fileName}`)

    const packageId = packageDescription.getAttribute('id')
    if(!packageId || !packageId.length)
        throw new Error(`Couldn't find id for package in folder ${this.basePath}`)

    this.packageId = packageId
    this.definitionsTree = new Tree(fileName)
    const resources = packageDescription.getElementsByTagName('resources')[0]

    for(let i = 0; i < resources.childNodes.length; i++){
        const node = resources.childNodes[i] as Element
        if(node.constructor.name == 'Element'){
            const nodeId = node.getAttribute('id')
            this.definitionsTree.addNode(node, this.packageId + nodeId)
        }
    }

    return true
}