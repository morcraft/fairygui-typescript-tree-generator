const ignoredTags = {
    Button: true,
    relation: true,
    ComboBox: true,
    Slider: true,
    gearXY: true,
    group: true
}

export type keys = keyof typeof ignoredTags

export default ignoredTags